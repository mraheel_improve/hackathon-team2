import Vue from 'vue';
import { AclInstaller, AclCreate, AclRule } from 'vue-acl';
import router from './router';

Vue.use(AclInstaller);

export default new AclCreate({
	initial: 'public',
	notfound: '/error',
	router,
	globalRules: {
		isPublic: new AclRule('public').generate(),
		isLogged: new AclRule('user').generate(),
	},
});
