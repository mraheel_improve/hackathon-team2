import Vue, { ComponentOptions } from "vue";
import { AclRule } from "vue-acl";

declare module "vue/types/acl-rule" {
  interface Vue {
    $aclRule: AclRule<any>;
}