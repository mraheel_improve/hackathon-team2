import Vue, { ComponentOptions } from "vue";
import Acl from "../acl";
import { AclCreate } from 'vue-acl';

declare module "vue/types/options" {
  interface ComponentOptions<V extends Vue> {
    acl?: AclCreate;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $acl: AclCreate;
}